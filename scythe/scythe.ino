/* StitchKit LED Lights example
 * -------------------------------
 *
 * This software demos LED effects from FastLED
 * NOTE: This demo code is from the FastLED Demo Reel 100 code, available when you download the library
 * To learn more about FastLED, go here: http://fastled.io/
 *
 * We have enabled the effects on both Pin 5 and 6 outputs on the StitchKit board.
 *
 *
 * Requires:
 * ---------
 *  MakeFashion controller board
 *  FastLED-compatible LEDs ( https://github.com/FastLED/FastLED/wiki/Overview#chipsets )
 *
 * Dependencies:
 * -------------
 *  FastLED ( Available via Arduino package manager )
 */

#include "FastLED.h"

FASTLED_USING_NAMESPACE

#if defined(FASTLED_VERSION) && (FASTLED_VERSION < 3001000)
#warning "Requires FastLED 3.1 or later; check github for latest code."
#endif

#define PIN_LEDARRAY2    6

#define LED_TYPE           WS2812B
#define COLOR_ORDER        GRB

//Change the number of LEDs here according to how many you have
// Long handle strip is LED 1 - 25
// Blade is LED 26 - 55
#define NUM_LEDS 55
#define NUM_LEDS_BLADE 30
#define NUM_LEDS_HANDLE 25

//Change the brightness of the LEDs here according to how many you have
#define BRIGHTNESS 255




#define MAX_POWER_MILLIWATTS 1000

CRGB leds[NUM_LEDS];
#define FRAMES_PER_SECOND 4

void setup() {
  delay(2000); // 3 second delay for recovery

  // Ok, so halving the possible power seems to work.
  FastLED.setMaxPowerInMilliWatts(MAX_POWER_MILLIWATTS);

  // tell FastLED about the LED strip configuration
  // WHat correction do we need? UncorrectedColor?
  FastLED.addLeds<LED_TYPE,PIN_LEDARRAY2,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);

  initBlade();
  animateHandle();
  FastLED.show();

  // set master brightness control
  FastLED.setBrightness(BRIGHTNESS);
}

void loop()
{
  // Call the current pattern function once, updating the 'leds' array


  // send the 'leds' array out to the actual LED strip
//  FastLED.show();
//  // insert a delay to keep the framerate modest
  FastLED.delay(1000/FRAMES_PER_SECOND);
  animateBlade();
//  animateHandle();
  // do some periodic updates
//  EVERY_N_MILLISECONDS( 20 ) { gHue++; } // slowly cycle the "base color" through the rainbow
   EVERY_N_MILLISECONDS( 500 ) { animateHandle(); }
   EVERY_N_MILLISECONDS( 500 ) { animateBlade(); }
   
   FastLED.show();

}


void initBlade() {
  setSlice(26, 55, CRGB::DarkRed);
}

void animateBlade()
{
  FadeInOut(0x88, 0x00, 0x00);
}

void animateHandle() {
  for(int i = 1; i <= 25; i++ ) {
    leds[i] = getWhite();
  }
}

CRGB getWhite() 
{
  fract8 chance = random8();

  if(chance < 50) {
    return CRGB::FairyLight;
  }

  if(chance < 90) {
    return CRGB::NavajoWhite;    
  }

  return CRGB::AntiqueWhite;
}

// Allows passing in directly CRGB::Colour
void setSlice(int begin, int end, long colour)
{
  for(int i = begin; i < end; i++ ) {
    leds[i] = colour;
  }
}

// Set the colour of led in a range, each individual pixel
void setSlice(int begin, int end, byte red, byte green, byte blue)
{
  for(int i = begin; i < end; i++ ) {
    setPixel(i, red, green, blue);
  }
}

void setPixel(int Pixel, byte red, byte green, byte blue)
{
  leds[Pixel].r = red;
  leds[Pixel].g = green;
  leds[Pixel].b = blue;
}

void FadeInOut(byte red, byte green, byte blue)
{
  float r, g, b;
  uint8_t lowend = 100;
  
  for (int k = lowend; k < 256; k = k + 1)
  {
    r = (k / 256.0) * red;
    g = (k / 256.0) * green;
    b = (k / 256.0) * blue;
    setSlice(26, 55, r, g, b);
    showStrip();
  }

  for (int k = 255; k >= lowend; k = k - 2)
  {
    r = (k / 256.0) * red;
    g = (k / 256.0) * green;
    b = (k / 256.0) * blue;
    setSlice(26, 55, r, g, b);
    showStrip();
  }
}

void showStrip()
{
   FastLED.show();
}
